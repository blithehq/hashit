HashIt

About

HashIt is an application to gather the photos shared in Facebook, Twitter and Instagram.

Project Features

Phase 1

	1. Organiser can sign-up / login and create events.
	2. Organiser will share the event hashtag by offline.
	3. A scheduler will grab the photos(only 75) from social networks under the event on daily basis.
	4. The photos will be displayed in the organiser landing page.
	
Phase 2
	
	1. Payment option for organiser for all photos.
	2. Scheduler to run every hour for paid users.
	
Phase 3
	
	1. Mobile application (Technologies not yet decided).

Tools to be used

	1. PHP
	2. MYSQL
	3. jQuery
	4. HTML5 
	5. CSS Bootstrap framework
	
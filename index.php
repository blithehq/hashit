<?php include_once "header.php"; ?>
<div class="wrapper"> 

	<div id="headerwrap">
		<div class="container">
			<div class="row">
				<div class="col-lg-6">
					<h1>Gather photos across the social networking websites</h1>
					<form class="form-inline" role="form">
					  <a href="Register" class="btn btn-warning btn-lg">Get Started Today</a>
					</form>					
				</div><!-- /col-lg-6 -->
				<div class="col-lg-6">
					<img class="img-responsive" src="assets/img/ipad-hand.png" alt="">
				</div><!-- /col-lg-6 -->
				
			</div><!-- /row -->
		</div><!-- /container -->
	</div><!-- /headerwrap -->
	
	
	<div class="container" id="Features">
		<div class="row mt centered">
			<div class="col-lg-6 col-lg-offset-3">
				<h1>Title</h1>
				<h3>Content to be placed</h3>
			</div>
		</div><!-- /row -->
	</div><!-- /container -->
	
	<div class="container" id="Pricing">
		<hr>
		<div class="row mt centered">
			<div class="col-lg-6 col-lg-offset-3">
				<h1>Pricing</h1>
				<h3>Content to be placed</h3>
			</div>
		</div><!-- /row -->
	</div><!-- /container -->
	
</div>
<?php include_once "footer.php"; ?>
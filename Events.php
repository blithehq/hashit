<?php include_once "header.php";?>
	
<div class="clear20"></div>
<div class="wrapper">	
	<div class="container">
		<div class="row mt">
			
			<div class="col-sm-5">
				<h2>Events</h2>
				<div class="basic-login">
					<table width="100%">
					<tr>
						<td>
							<a href="photo_feed.html">event1</a>
						</td>
						<td align="right">
							<a href="photo_feed.html">edit</a> | <a href="photo_feed.html">delete</a>	
						</td>
					</tr>
					<tr>
						<td>
							<a href="photo_feed.html">event2</a>
						</td>
						<td align="right">
							<a href="photo_feed.html">edit</a> | <a href="photo_feed.html">delete</a>
						</td>
					</tr>
					</table>
				</div>
			</div>
			<div class="col-sm-7 social-login">
				<p>&nbsp;</p>
				<div class="form-group">
					
					<a href="create_event.html" class="btn btn-warning btn-lg">Set up an Event</a>
					<div class="clearfix"></div>
				</div>
			</div>
		
		</div><!-- /row -->
	</div><!-- /container -->
</div>
<?php include_once "footer.php";?>
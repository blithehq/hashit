<?php 
	include_once "header.php"; 
	echo "<pre>";
	print_r($_POST);
	echo "</pre>";

	if($userobj->getVariable("Email")!=""){
		$data = array();
		$User = new User();
		$UserExists = $User->UserExists($userobj->getVariable("Email"));
		if($UserExists > 0){
			echo "<script language='JavaScript'>alert('Email Already Exists')</script>";
		} else {
			$data["FirstName"] = $userobj->getVariable("FirstName");
			$data["LastName"] = $userobj->getVariable("LastName");
			$data["Email"] = $userobj->getVariable("Email");
			$data["Password"] = $userobj->getVariable("Password");

			$User = new User();
			$UserId = $User->Register($data);
			
			echo "<script language='JavaScript'>window.location.href='Confirmation'</script>";die;
		}
	}

?>

<script language="Javascript">
  $(function () { $("input,select,textarea").not("[type=submit]").jqBootstrapValidation(); } );
</script>

<div class="clear20"></div>
<div class="wrapper">	
	<div class="container">
		<div class="row mt">
			
			<div class="col-sm-5">
				<h2>Register</h2>
				<div class="basic-login">
					<form role="form" name="Register" id="Register" method="POST" action="Register">

						<div class="form-group">
        				 	<label for="login-firstname">Firstname</label>
							<input name="FirstName" class="form-control" id="login-firstname" type="text" required data-validation-required-message="Please fill out this field" />
						</div>
						<div class="form-group">
        				 	<label for="login-lastname">Lastname</label>
							<input name="LastName" class="form-control" id="login-lastname" type="text" required data-validation-required-message="Please fill out this field" />
						</div>
						<div class="form-group">
        				 	<label for="login-username">Email</label>
							<input name="Email" class="form-control" id="login-username" type="email" required data-validation-required-message="Please fill out this field" />
						</div>
						<div class="form-group">
        				 	<label for="login-password">Password</label>
							<input name="Password" class="form-control" id="login-password" type="password" placeholder="Min. 8 characters" />
						</div>
						<div class="form-group">
        				 	<label for="login-repassword">Re-enter Password</label>
							<input name="passwordagain" class="form-control" id="login-repassword" type="password" placeholder="Min. 8 characters" data-validation-matches-match="Password" data-validation-matches-message="Must match password entered above" />
						</div>

						<div class="form-group">
							<label class="checkbox">
								<input type="checkbox" name="TermsAndConditions" required data-validation-required-message="You must agree to the terms and conditions" /> 
								<span style="font-size:16px;font-weight:normal;">I agree to the <a href="#">terms & conditions</a></span>
							</label>
							<input type="submit" name="Submit" value="Register" class="btn btn-warning btn-lg pull-right"></a>
							<div class="clearfix"></div>
						</div>
					</form>
				</div>
			</div>
			<div class="col-sm-7 social-login">
				<p>You can use your Facebook or Twitter for registration</p>
				<div class="social-login-buttons">
					<a href="#" class="btn-facebook-login">Use Facebook</a>
					<a href="#" class="btn-twitter-login">Use Twitter</a>
				</div>
				
			</div>
		
		</div><!-- /row -->
	</div><!-- /container -->
</div>
<?php include_once "footer.php"; ?>
<?php
//ini_set('display_errors', '1');
//error_reporting(E_ERROR | E_WARNING | E_PARSE);
require_once "includes/config.php";
require_once "includes/session.inc";
require_once "includes/mysql.inc";
require_once "includes/db.inc";
require_once "includes/userfunctions.inc";

//$sqlobj = new dbconn(DBHOST, DBUSER, DBPASS, DBNAME);
$sesobj = new SESSION_MANAGEMENT();
$userobj = new UserFunctions();

foreach (glob("classes/*.php") as $filename)
{
	include $filename;
}
?>